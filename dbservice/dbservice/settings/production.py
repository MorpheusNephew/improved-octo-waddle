from .base import *
from decouple import config
from dj_database_url import parse
from pathlib import Path

# Middleware

MIDDLEWARE = MIDDLEWARE + ["django.middleware.security.SecurityMiddleware",
                           "whitenoise.middleware.WhiteNoiseMiddleware",]

# Database
# https://docs.djangoproject.com/en/4.2/ref/settings/#databases

DB_NAME = config("DB_NAME")
DB_USER = config("DB_USER")
DB_PASSWORD = config("DB_PASSWORD")
DB_HOST = config("DB_HOST")
DB_PORT = config("DB_PORT")

DATABASES = {
    "default": parse(f"postgres://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}",
                     engine="django.db.backends.postgresql",
                     conn_max_age=10,
                     conn_health_checks=True)
}

STATIC_ROOT = Path.joinpath(BASE_DIR, 'static')
