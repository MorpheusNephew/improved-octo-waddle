#!/bin/bash

echo "Migrate the Database at startup of project"

# Wait for few minute and run db migraiton
while ! python manage.py migrate 2>&1; do
   echo "Migration is in progress status"
   sleep 3
done

# Wait for collect static
while ! python manage.py collectstatic --no-input 2>&1; do
   echo "Collecting static"
   sleep 3
done

echo "Django docker is fully configured successfully."

exec "$@"
