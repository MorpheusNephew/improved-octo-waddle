from typing import Any
from django.core.management.base import BaseCommand
from ...queue import IngestSubscription


def main():
    IngestSubscription.start_consuming()


class Command(BaseCommand):
    help = 'Starts ingestion of player game stats'

    def handle(self, *args: Any, **options: Any) -> str | None:
        main()
