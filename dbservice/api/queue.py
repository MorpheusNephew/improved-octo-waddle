import json
from typing import Any
from decouple import config
from pika import BasicProperties, ConnectionParameters, DeliveryMode, channel, PlainCredentials, BlockingConnection, SelectConnection, BaseConnection
from enum import Enum
from .models import PlayerGameStat


def get_connection_parameters() -> ConnectionParameters:
    credentials = PlainCredentials('guest', 'guest')
    return ConnectionParameters(
        host=config('RABBITMQ_HOST', default='localhost'), port=config('RABBITMQ_PORT', default='5672'), virtual_host='/', credentials=credentials, blocked_connection_timeout=300, heartbeat=600)


class ConnectionType(Enum):
    BLOCKING = 1
    SELECT = 2


class QueueConnection():
    __connection: BaseConnection = None

    def __init__(self, connection_type: ConnectionType):

        if QueueConnection.__connection is None or not QueueConnection.__connection.is_open:
            connection_parameters = get_connection_parameters()

            match connection_type:
                case ConnectionType.BLOCKING:
                    QueueConnection.__connection = BlockingConnection(
                        connection_parameters)

                case ConnectionType.SELECT:
                    QueueConnection.__connection = SelectConnection(
                        connection_parameters)
                case _:
                    raise Exception('Connection type not provided')

        else:
            raise Exception('Cannot instantiate a new QueueConnection')

    @staticmethod
    def get_connection(connection_type: ConnectionType) -> BaseConnection:

        if QueueConnection.__connection is None or not QueueConnection.__connection.is_open:
            QueueConnection(connection_type)

        return QueueConnection.__connection


class QueueConnectionChannel():
    __channel: channel.Channel = None

    def __init__(self, queue: Any):

        if QueueConnectionChannel.__channel is None or not QueueConnectionChannel.__channel.is_open:
            connection = QueueConnection.get_connection(
                ConnectionType.BLOCKING)

            QueueConnectionChannel.__channel = connection.channel()

            QueueConnectionChannel.__channel.queue_declare(
                queue)
        else:
            raise Exception('Cannot instantiate a new QueueConnectionChannel')

    @staticmethod
    def get_channel(queue: Any) -> channel.Channel:

        if QueueConnectionChannel.__channel is None or not QueueConnectionChannel.__channel.is_open:
            QueueConnectionChannel(queue)

        return QueueConnectionChannel.__channel


class IngestSubscription:
    """Ingests player game stats data into database
    """

    @staticmethod
    def start_consuming():
        def callback(ch, method, properties, body):
            json_body = json.loads(body)
            default_copy_body = json_body['data'].copy()
            query_filters = {
                'gameId': default_copy_body['gameId'], 'playerId': default_copy_body['playerId']}
            del default_copy_body['gameId']
            del default_copy_body['playerId']

            print(f'This is a copy {default_copy_body}')

            player_game_stats, created = PlayerGameStat.objects.update_or_create(
                **query_filters, defaults=default_copy_body)
            print(f'Player Game Stats: {player_game_stats}')
            print(f'Created: {created}')

        connection = QueueConnection.get_connection(ConnectionType.BLOCKING)

        print('Connection has to reach here after all errors are done')

        channel = connection.channel()

        channel.basic_consume(queue=config('RABBITMQ_DB_INGEST_QUEUE', default='Ingest'),
                              on_message_callback=callback, auto_ack=True)

        channel.start_consuming()


class IngestPublish:
    """Publish events for ingestion to start
    """

    @staticmethod
    def publish(data: Any):
        queue = config('RABBITMQ_INGEST_QUEUE', default='Ingest')

        channel = QueueConnectionChannel.get_channel(queue)

        channel.queue_declare(
            queue)

        channel.basic_publish('', queue, json.dumps({'data': data, 'pattern': 'load_players_stats'}),
                              BasicProperties(content_type='application/json', delivery_mode=DeliveryMode.Transient))
