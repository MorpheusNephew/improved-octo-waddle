from graphene import ObjectType, String, List, InputObjectType, Int, Enum, relay
from graphene_django import DjangoObjectType

from api.models import PlayerGameStat


class CompareOperatorQueryInput(Enum):
    """Comparison operators
    """

    EQ = "eq"
    LT = "lt"
    LTE = "lte"
    GT = "gt"
    GTE = "gte"

    def __str__(self):
        match self:
            case CompareOperatorQueryInput.EQ:
                return 'eq'
            case CompareOperatorQueryInput.GT:
                return 'gt'
            case CompareOperatorQueryInput.GTE:
                return 'gte'
            case CompareOperatorQueryInput.LT:
                return 'lt'
            case CompareOperatorQueryInput.LTE:
                return 'lte'


class PlayerPositionsQueryInput(Enum):
    """Player positions
    """

    GOALIE = "Goalie"
    DEFENSEMAN = "Defenseman"
    RIGHT_WING = "Right Wing"
    LEFT_WING = "Left Wing"
    CENTER = "Center"
    UNKNOWN = "Unknown"


class StatisticsQueryInput(InputObjectType):
    """Input used to search for a value via an operator
    """

    value = Int(required=True, description="Value to search")

    operator = CompareOperatorQueryInput(
        required=True, description="Operator to compare value")


class PlayerGameStatsQueryInput(InputObjectType):
    """Input used to query player stats for games
    """

    gameIds = List(Int, description="A list of game ids to search")

    playerIds = List(Int, description="A list of player ids to search")

    teamIds = List(Int, description="A list of team ids to search")

    opponentTeamIds = List(
        Int, description="A list of opponent team ids to search")

    playerNumbers = List(
        String, description="A list of player numbers to search")

    playerPositions = List(PlayerPositionsQueryInput,
                           description="A list of player positions to search")

    playerAge = StatisticsQueryInput(description="Player ages to search")

    assists = StatisticsQueryInput(description="Player assists to search")

    goals = StatisticsQueryInput(description="Player goals to search")

    hits = StatisticsQueryInput(description="Player hits to search")

    points = StatisticsQueryInput(description="Player points to search")


class PlayerGameStatsType(DjangoObjectType):
    class Meta:
        model = PlayerGameStat
        fields = "__all__"
        interfaces = (relay.Node,)


class PlayerGameStatsConnection(relay.Connection):
    class Meta:
        node = PlayerGameStatsType


class Query(ObjectType):
    player_game_stats = relay.ConnectionField(PlayerGameStatsConnection, input=PlayerGameStatsQueryInput())

    def resolve_player_game_stats(parent, info, input: PlayerGameStatsQueryInput, **args):
        all_stats = PlayerGameStat.objects.all()

        queries = [(input.gameIds, 'gameId'), (input.playerIds, 'playerId'), (input.teamIds, 'teamId'),
                   (input.opponentTeamIds,
                    'opponentTeamId'), (input.playerNumbers, 'playerNumber'),
                   (input.playerPositions, 'playerPosition')]

        for values, column_name in queries:
            if values is not None:
                all_stats = all_stats.filter(**{f'{column_name}__in': values})

        comparisons = [(input.assists, 'assists'), (input.hits, 'hits'), (input.goals, 'goals'),
                       (input.points, 'points')]

        for comparer, column_name in comparisons:
            if comparer is not None:
                match comparer.operator:
                    case CompareOperatorQueryInput.EQ:
                        all_stats = all_stats.filter(
                            **{column_name: comparer.value})
                    case _:
                        all_stats = all_stats.filter(
                            **{f'{column_name}__{comparer.operator}': comparer.value})

        return all_stats
