from django.contrib import admin
from .models import PlayerGameStat


class PlayerGameStatAdmin(admin.ModelAdmin):
    list_display = ["gameId", "playerName", "teamName", "opponentTeamName"]


admin.site.register(PlayerGameStat, PlayerGameStatAdmin)
