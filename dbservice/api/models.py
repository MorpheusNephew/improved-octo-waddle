from django.db import models


class PlayerGameStat(models.Model):
    PLAYER_POSITION_CHOICES = (("Goalie", "Goalie"), ("Defenseman", "Defenseman"), (
        "Right Wing", "Right Wing"), ("Left Wing", "Left Wing"), ("Center", "Center"), ("Unknown", "Unknown"))

    gameId = models.IntegerField()
    playerId = models.IntegerField()
    playerName = models.TextField(null=True)
    teamId = models.IntegerField()
    teamName = models.TextField()
    playerAge = models.IntegerField(null=True)
    playerNumber = models.TextField(null=True)
    playerPosition = models.TextField(choices=PLAYER_POSITION_CHOICES)
    assists = models.IntegerField()
    goals = models.IntegerField()
    hits = models.IntegerField(default=0)
    points = models.IntegerField()
    penaltyMinutes = models.IntegerField(default=0)
    opponentTeamName = models.TextField()
    opponentTeamId = models.IntegerField()

    class Meta:
        constraints = [models.UniqueConstraint(
            name="unique_player_game_stats", fields=["gameId", "playerId"])]

        ordering = ['gameId', 'playerId']
