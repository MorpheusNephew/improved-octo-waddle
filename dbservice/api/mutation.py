from graphene import ObjectType, Mutation, Int, String, InputObjectType

from .queue import IngestPublish


class StatsInput(InputObjectType):
    game_id = Int()
    season_id = String()


class LoadPlayerStats(Mutation):
    class Arguments:
        stat_input = StatsInput(required=True)

    message = String()

    def mutate(root, info, stat_input: StatsInput):
        print(f'Mutate input: {stat_input}')

        if stat_input.game_id is not None and stat_input.season_id is not None:
            return LoadPlayerStats(message='Cannot accept season and game for request')
        elif stat_input.game_id is not None:
            IngestPublish.publish(
                {'type': 'game', 'typeId': stat_input.game_id})
            return LoadPlayerStats(message=f'Ingesting game with id: {stat_input.game_id}')
        else:
            IngestPublish.publish(
                {'type': 'season', 'typeId': stat_input.season_id})
            return LoadPlayerStats(message=f'Ingesting season with id: {stat_input.season_id}')


class Mutation(ObjectType):
    load_player_stats = LoadPlayerStats.Field()
