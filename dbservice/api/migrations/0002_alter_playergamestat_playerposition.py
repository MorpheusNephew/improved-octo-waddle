# Generated by Django 4.2.5 on 2023-09-26 20:30

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('api', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='playergamestat',
            name='playerPosition',
            field=models.TextField(choices=[('Goalie', 'Goalie'), ('Defenseman', 'Defenseman'), ('Right Wing', 'Right Wing'), ('Left Wing', 'Left Wing'), ('Center', 'Center'), ('Unknown', 'Unknown')]),
        ),
    ]
