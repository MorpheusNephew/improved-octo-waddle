import { Test, TestingModule } from '@nestjs/testing';
import { GameService } from './game.service';
import { NhlService } from '../nhl/nhl.service';
import { HttpModule } from '@nestjs/axios';

describe('GameService', () => {
  let service: GameService;

  beforeEach(async () => {
    jest.restoreAllMocks();

    const module: TestingModule = await Test.createTestingModule({
      imports: [HttpModule],
      providers: [
        GameService,
        NhlService,
      ],
    }).compile();

    service = module.get<GameService>(GameService);
  });

  describe('load', () => {});
});
