import { Module } from '@nestjs/common';
import { GameService } from './game.service';
import { NhlModule } from '../nhl/nhl.module';
import { ClientsModule, Transport } from '@nestjs/microservices';

@Module({
  imports: [
    NhlModule,
    ClientsModule.register([
      {
        name: 'DB_INGEST_SERVICE',
        transport: Transport.RMQ,
        options: {
          urls: [
            `amqp://${process.env.RABBITMQ_HOST}:${process.env.RABBITMQ_PORT}`,
          ],
          queue: process.env.RABBITMQ_DB_INGEST_QUEUE,
          queueOptions: {
            durable: false,
          },
        },
      },
    ]),
  ],
  exports: [
    GameService,
    NhlModule,
    ClientsModule,
  ],
  providers: [GameService],
})
export class GameModule { }
