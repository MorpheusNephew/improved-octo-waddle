import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { SeasonModule } from './season/season.module';
import { ConfigModule } from '@nestjs/config';

@Module({
  imports: [
    ConfigModule.forRoot({ envFilePath: ['.env'] }),
    SeasonModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
